#include <Messaging/Channel.hpp>
#include <Core/Module.hpp>
#include <Core/Engine.hpp>
#include <Video/Video.hpp>
#include <ECS/Manager.hpp>

#include <ECS/Components/Camera.hpp>
#include <ECS/Components/Transform.hpp>

extern "C" {
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
}


int main() {

    Romuva::Core::gEngine.addModule(new Romuva::Video::RenderModule);
    if (!Romuva::Core::gEngine.init()) {
        return -1;
    }

    std::string meshPath = "data/models/teapot.fbx";
    Romuva::Video::RenderModule::LoadMesh loadMesh{meshPath};
    Romuva::Messaging::Channel::broadcast(loadMesh);

    Romuva::Video::RenderModule::LoadShader loadShader{"data/shaders/default_vertex_shader.glsl", "data/shaders/default_fragment_shader.glsl"};
    Romuva::Messaging::Channel::broadcast(loadShader);

    // Create temporary camera entity
    auto* entityManager = Romuva::Core::gEngine.getModule<Romuva::ECS::Manager>();
    Romuva::ECS::Entity& cameraEntity = entityManager->addEntity();
    cameraEntity.addComponent<Romuva::Components::Transform>(
            Romuva::Vector3f{0, 0, 0},
            Romuva::Vector3f{0, 1, 0},
            Romuva::Vector3f{0, 0, 1}
            );
    cameraEntity.addComponent<Romuva::Camera>(
            true, 45.0f, 0.1f, 9999.f, Romuva::Camera::PERSPECTIVE
            );

    Romuva::Messaging::Channel::broadcast(Romuva::Video::RenderModule::SetRenderCamera{&cameraEntity.getComponent<Romuva::Camera>()});


    Romuva::Core::gEngine.run();

    gLogDebug << " TESTING LUAJIT: ";
    lua_State* lstate = nullptr;
    lstate = lua_open();
    luaL_openlibs(lstate);
    luaL_dostring(lstate, "return 2 + 3");
    gLogDebug << " Possible output: " << lua_tostring(lstate, -1);
    lua_close(lstate);

    return 0;
}