//
// Created by kestutis on 18.6.13.
//

#include "Video.hpp"

#include <Core/Logging/Logger.hpp>
#include <Core/Engine.hpp>

namespace Romuva {

    namespace Video {

        RenderModule::RenderModule() : Module("RenderModule", true, false), _window(nullptr), _keepRendering(false), loadedShader{}, _gBuffer{}, _camera(nullptr) {
            Messaging::Channel::add<LoadMesh>(this);
            Messaging::Channel::add<LoadShader>(this);
            Messaging::Channel::add<SetRenderCamera>(this);
        }
        RenderModule::~RenderModule() = default;

        bool RenderModule::init() {

            glfwSetErrorCallback(RenderModule::errorCallback);

            if (!glfwInit()) {
                gLogFatal << "Unable to initialise GLFW!";
                return false;
            }

            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

            this->_window = glfwCreateWindow(1280, 720, "Romuva", nullptr, nullptr);
            if (!this->_window) {
                gLogFatal << "Unable to create GLFW window!";
                glfwTerminate();
                return false;
            }

            glfwMakeContextCurrent(this->_window);

            if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress) ) {
                gLogFatal << "Failed to initialise OpenGL context";
                glfwDestroyWindow(this->_window);
                glfwTerminate();
                return false;
            }

            glViewport(0,0,1270,720);

            glFrontFace(GL_CW);
            glCullFace(GL_BACK);
            glEnable(GL_CULL_FACE);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LEQUAL);
            glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

            if (!_gBuffer.init(1270, 720)) {
                return false;
            }

            _keepRendering = true;

            return Core::Module::init();;
        }

        void RenderModule::update() {

            if (glfwWindowShouldClose(_window) && _keepRendering) {
                Romuva::Messaging::Channel::broadcast(Romuva::Core::Engine::OnStop());
                _keepRendering = false;
            }

            if (_keepRendering) {
                glfwPollEvents();

                _gBuffer.preRender();

                shadowMapPass(meshes, _camera);
                geometryPass(meshes, _camera);
                //spotlightPass()
                //pointlightPass()
                //directionallightPass();
                combinePasses();
                element2DPass();
                finalPass();

                glBindFramebuffer(GL_FRAMEBUFFER, 0);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, 0);

                glfwSwapBuffers(_window);

                sleep(1);
            }

            Core::Module::update();

        }

        void RenderModule::errorCallback(int error, const char *description) {
            gLogError << "OpenGL Error: " << error << " Description: " << description;
        }

        void RenderModule::shutdown() {
            meshes.clear();
            loadedShader.unload();

            if (_window) {
                glfwDestroyWindow(_window);
            }
            glfwTerminate();

            Core::Module::shutdown();
        }

        void RenderModule::operator()(const RenderModule::LoadMesh &data) {
            Mesh mesh;
            mesh.loadMesh(data.path);
            meshes.insert(mesh);

        }

        void RenderModule::operator()(const RenderModule::LoadShader &data) {
            loadedShader.unload();
            loadedShader.load(data.vsPath, data.fsPath);
        }

        void RenderModule::operator()(const RenderModule::SetRenderCamera &data) {
            _camera = data.camera;
        }

        void RenderModule::shadowMapPass(tbb::concurrent_unordered_set<Mesh, Mesh::hasher, Mesh::equal_to> set,
                                         Camera *pCamera) {
            glCullFace(GL_FRONT);

            // TODO
        }

        void RenderModule::geometryPass(tbb::concurrent_unordered_set<Mesh, Mesh::hasher, Mesh::equal_to> set,
                                        Camera *pCamera) {
            _gBuffer.geometryPass();

            glDepthMask(GL_TRUE);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);
            glCullFace(GL_BACK);

            Matrix4f view = pCamera->getView();
            Matrix4f projection = pCamera->getProjection();

            Matrix4f translation = Matrix4f::FromTranslationVector(Vector3f{0, 0, 10});
            Matrix4f scale = Matrix4f::FromScaleVector(Vector3f{1,1,1});
            Matrix4f rotation = Matrix4f::FromRotationMatrix( Quaternionf::FromEulerAngles(0,0,0).ToMatrix());

            Matrix4f modelMatrix = translation * rotation * scale;

            Matrix4f mvp = projection * view * modelMatrix;

            loadedShader.enable();
            loadedShader.setUniform<Matrix4f>("model", modelMatrix);
            loadedShader.setUniform<Matrix4f>("mvp", mvp);
            loadedShader.setUniform<Matrix4f>("view", view);
            loadedShader.setUniform<Matrix4f>("projection", projection);

            auto meshIt = meshes.begin();
            while(meshIt != meshes.end()) {
                (*meshIt).render();
                ++meshIt;
            }
            loadedShader.disable();

            glDepthMask(GL_FALSE);
        }

        void RenderModule::combinePasses() {

        }

        void RenderModule::element2DPass() {

        }

        void RenderModule::finalPass() {
            _gBuffer.finalPass();
            glBlitFramebuffer(0, 0, 1270, 720, 1270, 0, 0, 720, GL_COLOR_BUFFER_BIT, GL_LINEAR);
        }

    }

}