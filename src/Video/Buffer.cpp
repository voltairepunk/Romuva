//
// Created by voltaire on 5/4/19.
//

#include "Buffer.hpp"

#include <Core/Logging/Logger.hpp>


namespace Romuva {

    namespace Video {

        bool Buffer::init(int width, int height) {

            gLog << "Creating buffer. Width = " << width << " Height = " << height;

            glGenFramebuffers(1, &_fbo);
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);

            unsigned int texSize = sizeof(_textures) / sizeof(_textures[0]);
            glGenTextures(texSize, _textures);

            glGenTextures(1, &_depthTexture);
            glGenTextures(1, &_finalTexture);
            glGenTextures(1, &_forwardTexture);

            unsigned int i = 0;
            for (auto texture : _textures) {
                glBindTexture(GL_TEXTURE_2D, texture);
                glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i++, GL_TEXTURE_2D, texture, 0);
            }

            glBindTexture(GL_TEXTURE_2D, _depthTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH32F_STENCIL8, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, _depthTexture, 0);

            glBindTexture(GL_TEXTURE_2D, _forwardTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, _forwardTexture, 0);

            glBindTexture(GL_TEXTURE_2D, _finalTexture);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_FLOAT, nullptr);
            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, _finalTexture, 0);

            GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

            if (status != GL_FRAMEBUFFER_COMPLETE) {
                gLogFatal << "Could not initialise video buffer";
                return false;
            }

            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);

            gLog << "Done";

            return true;
        }

        void Buffer::destroy() {
            gLog << "Destroying video bufffer";
            if (_fbo != 0) glDeleteFramebuffers(1, &_fbo);
            if (_forwardTexture != 0) glDeleteFramebuffers(1, &_forwardTexture);
            if (_textures[0] != 0) glDeleteFramebuffers(sizeof(_textures) / sizeof(_textures[0]), _textures);
            if (_finalTexture != 0) glDeleteFramebuffers(1, &_finalTexture);
            if (_depthTexture != 0) glDeleteFramebuffers(1, &_depthTexture);
            gLog << "Done";
        }

        void Buffer::preRender() {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
            glDrawBuffer(GL_COLOR_ATTACHMENT5);
            glClear(GL_COLOR_BUFFER_BIT);
        }

        void Buffer::geometryPass() {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
            GLenum drawBuffer[] = {
                    GL_COLOR_ATTACHMENT0,
                    GL_COLOR_ATTACHMENT1,
                    GL_COLOR_ATTACHMENT3
            };

            glDrawBuffers(sizeof(drawBuffer) / sizeof(drawBuffer[0]), drawBuffer);
        }

        void Buffer::stencilPass() {
            glDrawBuffer(GL_NONE);
        }

        void Buffer::lightPass() {
            glDrawBuffer(GL_COLOR_ATTACHMENT5);
            unsigned int i = 0;
            for (auto texture: _textures) {
                glActiveTexture(GL_TEXTURE0 + i++);
                glBindTexture(GL_TEXTURE_2D, texture);
            }
        }

        void Buffer::materialPass() {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
            glDrawBuffer(GL_COLOR_ATTACHMENT4);
        }

        void Buffer::element2DPass() {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
            glDrawBuffer(GL_COLOR_ATTACHMENT5);
        }

        void Buffer::finalPass() {
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
            glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbo);
            glReadBuffer(GL_COLOR_ATTACHMENT5);
        }

        const GLuint &Buffer::getDepthTexture() {
            return _depthTexture;
        }

        const GLuint &Buffer::getForwardTexture() {
            return _forwardTexture;
        }
    }

}