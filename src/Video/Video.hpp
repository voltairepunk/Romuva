//
// Created by kestutis on 18.6.13.
//

#ifndef ROMUVA_VIDEO_HPP
#define ROMUVA_VIDEO_HPP

#include <Core/Module.hpp>
#include <ECS/Components/Camera.hpp>

#include <Video/Buffer.hpp>
#include <Video/Components/Mesh.hpp>
#include <Video/Components/Shader.hpp>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <tbb/concurrent_unordered_set.h>

namespace Romuva {

    namespace Video {


        class RenderModule : public Core::Module
        {
        public:

            struct LoadMesh {
                std::string path;
            };
            struct LoadShader {
                std::string vsPath;
                std::string fsPath;
            };
            struct SetRenderCamera {
                Camera* camera;
            };

            RenderModule();
            ~RenderModule() override;

            bool init() override;
            void update() override;
            void shutdown() override;

            static void errorCallback(int error, const char* description);

            void operator()(const LoadMesh& data);
            void operator()(const LoadShader& data);
            void operator()(const SetRenderCamera& data);

        protected:

            GLFWwindow* _window;
            Camera* _camera;
            Buffer _gBuffer;
            bool _keepRendering;

            tbb::concurrent_unordered_set<Mesh, Mesh::hasher, Mesh::equal_to> meshes;
            Shader loadedShader;

            void shadowMapPass(tbb::concurrent_unordered_set<Mesh, Mesh::hasher, Mesh::equal_to> set, Camera *pCamera);
            void geometryPass(tbb::concurrent_unordered_set<Mesh, Mesh::hasher, Mesh::equal_to> set, Camera *pCamera);
            void combinePasses();
            void element2DPass();
            void finalPass();
        };


    }

}


#endif //ROMUVA_VIDEO_HPP
