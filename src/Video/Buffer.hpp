//
// Created by voltaire on 5/4/19.
//

#ifndef ROMUVA_BUFFER_HPP
#define ROMUVA_BUFFER_HPP

#include <Util/OpenGLIncludes.hpp>

namespace Romuva {

    namespace Video {

        class Buffer {

        public:

            enum {
                TEXTURE_TYPE_POSITION,
                TEXTURE_TYPE_DIFFUSE,
                TEXTURE_TYPE_NORMAL,
                NUM_TEXTURES
            };

            Buffer() = default;
            ~Buffer() = default;

            bool init(int width, int height);
            void destroy();

            void preRender();
            void geometryPass();
            void stencilPass();
            void lightPass();
            void materialPass();
            void element2DPass();
            void finalPass();

            const GLuint& getDepthTexture();
            const GLuint& getForwardTexture();

        protected:

            GLuint _fbo{};
            GLuint _textures[NUM_TEXTURES]{};
            GLuint _depthTexture{};
            GLuint _finalTexture{};
            GLuint _forwardTexture{};
        };

    }

}




#endif //ROMUVA_BUFFER_HPP
