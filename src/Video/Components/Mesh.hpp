//
// Created by voltaire on 4/28/19.
//

#ifndef ROMUVA_MESH_HPP
#define ROMUVA_MESH_HPP

#include <vector>
#include <rapidjson/document.h>
#include "../../Util/Mappings.hpp"
#include "../../Util/OpenGLIncludes.hpp"

namespace Romuva {

    namespace Video {

        class Mesh {

        public:

            Mesh();
            ~Mesh();


            bool loadMesh(const std::string& path);
            void unload();

            void setTextureId(const GLuint& textureId);
            void setNormalId(const GLuint& normalId);
            void setMaterialId(const GLuint& materialId);

            const std::string& getReference() const;

            void render();

            const GLuint& getTextureId();
            const GLuint& getNormalId();
            const GLuint& getMaterialId();

            struct hasher
            {
                size_t operator()(const Mesh& mesh) const;
            };

            struct equal_to
            {
                bool operator()(const Mesh& a, const Mesh& b) const;
            };

        protected:

            struct MeshEntry {
                MeshEntry();
                ~MeshEntry();

                void init(const std::vector<Vertex3f>& vertices, const std::vector<unsigned int>& indices);
                void unload();

                unsigned int indexCount;
                GLuint vb;
                GLuint ib;
                bool loaded;
            };

            GLuint _textureId;
            GLuint _normalId;
            GLuint _materialId;

            std::string _reference;

            std::vector<MeshEntry> _meshEntries;


        };

    }

}


#endif //ROMUVA_MESH_HPP
