//
// Created by voltaire on 4/28/19.
//

#include "Mesh.hpp"

#include <Util/Random.hpp>
#include <Core/Logging/Logger.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace Romuva {

    namespace Video {


        Mesh::Mesh() :
                _textureId(0),
                _normalId(0),
                _materialId(0),
                _reference(Util::Random::generate<std::string>(64)){

            gLogDebug << "Generated reference: " << _reference;

        }

        Mesh::~Mesh() {

        }

        bool Mesh::loadMesh(const std::string &path) {

            Assimp::Importer importer;
            const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate);

            if (!scene) {
                gLogError << "Error while importing file: " << importer.GetErrorString();
                return false;
            }

            bool hasTangents = false, hasNormals = false;

            for (unsigned int i = 0; i < scene->mNumMeshes; i++){
                if (scene->mMeshes[i]->HasNormals() && !hasNormals) {
                    hasNormals = true;
                }
                if (scene->mMeshes[i]->HasTangentsAndBitangents() && !hasTangents) {
                    hasTangents = true;
                }
                if (hasNormals && hasTangents) {
                    break;
                }
            }

            if (hasNormals) {
                scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_GenNormals);
            }

            if (hasTangents) {
                scene = importer.ApplyPostProcessing(aiPostProcessSteps::aiProcess_CalcTangentSpace);
            }

            _meshEntries.resize(scene->mNumMeshes);
            for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
                const aiMesh* mesh = scene->mMeshes[i];

                std::vector<Vertex3f> vertices;
                std::vector<unsigned int> indices;

                for (unsigned int j = 0; j < mesh->mNumVertices; j++) {
                    const aiVector3D& pos = mesh->mVertices[j];
                    const aiVector3D& normal = mesh->mNormals[j];

                    Vertex3f vertex;
                    vertex.position.x = pos.x;
                    vertex.position.y = pos.y;
                    vertex.position.z = pos.z;

                    vertex.normal.x = normal.x;
                    vertex.normal.y = normal.y;
                    vertex.normal.z = normal.z;

                    if (mesh->HasTextureCoords(0)) {
                        vertex.textureCoordinate.x = mesh->mTextureCoords[0][j].x;
                        vertex.textureCoordinate.y = mesh->mTextureCoords[0][j].y;
                    }

                    if (mesh->HasTangentsAndBitangents()) {
                        vertex.tangent.x = mesh->mTangents[j].x;
                        vertex.tangent.y = mesh->mTangents[j].y;
                        vertex.tangent.z = mesh->mTangents[j].z;
                    }

                    vertices.push_back(vertex);
                }

                for (unsigned int j = 0; j < mesh->mNumFaces; j++) {
                    const aiFace& face = mesh->mFaces[j];
                    if (face.mNumIndices == 3) {
                        indices.push_back(face.mIndices[0]);
                        indices.push_back(face.mIndices[1]);
                        indices.push_back(face.mIndices[3]);
                    }
                }

                this->_meshEntries[i].init(vertices, indices);
            }

            return true;
        }

        void Mesh::unload() {
            _textureId = 0;
            _normalId = 0;
            _materialId = 0;

            while (!_meshEntries.empty()) {
                _meshEntries.back().unload();
                _meshEntries.pop_back();
            }
        }

        void Mesh::setTextureId(const GLuint &textureId) {
            _textureId = textureId;
        }

        void Mesh::setNormalId(const GLuint &normalId) {
            _normalId = normalId;
        }

        void Mesh::setMaterialId(const GLuint &materialId) {
            _materialId = materialId;
        }

        const GLuint &Mesh::getTextureId() {
            return _textureId;
        }

        const GLuint &Mesh::getNormalId() {
            return _normalId;
        }

        const GLuint &Mesh::getMaterialId() {
            return _materialId;
        }

        void Mesh::render() {
            glEnable(GL_TEXTURE_2D);

            glEnableVertexAttribArray(0);
            glEnableVertexAttribArray(1);
            glEnableVertexAttribArray(2);
            glEnableVertexAttribArray(3);

            for (const auto &it : _meshEntries) {
                glBindBuffer(GL_ARRAY_BUFFER, it.vb);
                glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), 0);
                glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid *) 12);
                glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid *) 20);
                glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex3f), (const GLvoid *) 32);

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, it.ib);

                glEnableClientState(GL_TEXTURE_COORD_ARRAY);
                glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex3f), (const GLvoid *) 12);

                if (_textureId != 0) {
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, _textureId);
                }

                if (_normalId != 0) {
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, _normalId);
                }

                if (_materialId != 0) {
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, _materialId);
                }

                glDrawElements(GL_TRIANGLES, it.indexCount, GL_UNSIGNED_INT, 0);
                glDisableClientState(GL_TEXTURE_COORD_ARRAY);

                glActiveTexture(GL_TEXTURE0);
            }

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
            glDisableVertexAttribArray(2);
            glDisableVertexAttribArray(3);

            glDisable(GL_TEXTURE_2D);
        }

        const std::string &Mesh::getReference() const {
            return _reference;
        }


        Mesh::MeshEntry::MeshEntry() : ib(0xFFFFFFFF), vb(0xFFFFFFFF), indexCount(0), loaded(false) {

        }

        Mesh::MeshEntry::~MeshEntry() {
            if (loaded) {
                unload();
            }
        }

        void Mesh::MeshEntry::init(const std::vector<Vertex3f> &vertices, const std::vector<unsigned int> &indices) {
            indexCount = indices.size();

            glGenBuffers(1, &vb);
            glBindBuffer(GL_ARRAY_BUFFER, vb);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex3f) * indices.size(), &vertices[0], GL_STATIC_DRAW);

            glGenBuffers(1, &ib);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indexCount, &indices[0], GL_STATIC_DRAW);

            loaded = true;
        }

        void Mesh::MeshEntry::unload() {
            if (vb != 0xFFFFFFFF) {
                glDeleteBuffers(1, &vb);
            }

            if (ib != 0xFFFFFFFF) {
                glDeleteBuffers(1, &ib);
            }

            indexCount = 0;
            loaded = false;
        }

        size_t Mesh::hasher::operator()(const Mesh &mesh) const {
            std::hash<std::string> hash_string;
            return hash_string(mesh.getReference());
        }

        bool Mesh::equal_to::operator()(const Mesh &a, const Mesh &b) const {
            return (a.getReference() == b.getReference());
        }
    }

}