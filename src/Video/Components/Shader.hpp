//
// Created by voltaire on 5/1/19.
//

#ifndef ROMUVA_SHADER_HPP
#define ROMUVA_SHADER_HPP

#include <string>
#include <Util/OpenGLIncludes.hpp>

namespace Romuva {

    namespace Video {

        class Shader {
        public:

            Shader();
            ~Shader();

            bool load(const std::string& vsPath, const std::string& fsPath);
            void unload();

            void enable();
            void disable();

            template <typename T>
            void setUniform(std::string name, T value);

        protected:

            std::string loadFile(const std::string& path);
            void reportShaderError(GLuint shader);

            GLuint _program;

            GLuint _vertexShader;
            GLuint _fragmentShader;

        };

    }
}


#endif //ROMUVA_SHADER_HPP
