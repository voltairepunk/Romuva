//
// Created by voltaire on 5/1/19.
//

#include "Shader.hpp"

#include <Core/Logging/Logger.hpp>
#include <Util/Mappings.hpp>

#include <fstream>


namespace Romuva {

    namespace Video {


        Shader::Shader() : _program(0), _vertexShader(0), _fragmentShader(0) {

        }

        Shader::~Shader() {
            unload();
        };

        bool Shader::load(const std::string& vsPath, const std::string& fsPath) {

            // crate shader program
            _program = glCreateProgram();

            // load the actual shaders
            _vertexShader = glCreateShader(GL_VERTEX_SHADER);
            _fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

            std::string vsContents = loadFile(vsPath);
            std::string fsContents = loadFile(fsPath);

            const char* vsPathPtr = vsContents.c_str();
            const char* fsPathPtr = fsContents.c_str();
            int vsPathSize = vsContents.size();
            int fsPathSize = fsContents.size();
            GLint status = GL_FALSE;
            GLuint error;

            glShaderSource(_vertexShader, 1, &vsPathPtr, &vsPathSize);
            glShaderSource(_fragmentShader, 1, &fsPathPtr, &fsPathSize);

            glCompileShader(_vertexShader);
            glGetShaderiv(_vertexShader, GL_COMPILE_STATUS, &status);
            error = glGetError();
            if (status == GL_FALSE) {
                gLogError << "Could not compile vertex shader: " << error << " " << vsPath;
                reportShaderError(_vertexShader);
                return false;
            }

            glCompileShader(_fragmentShader);
            glGetShaderiv(_fragmentShader, GL_COMPILE_STATUS, &status);
            error = glGetError();
            if (status == GL_FALSE) {
                gLogError << "Could not compile fragment shader: " << error << " " << fsPath;
                reportShaderError(_fragmentShader);
                return false;
            }

            // Link to program
            glAttachShader(_program, _vertexShader);
            glAttachShader(_program, _fragmentShader);

            return true;
        }

        void Shader::enable() {
            glUseProgram(_program);
        }

        void Shader::disable() {
            glUseProgram(0);
        }

        std::string Shader::loadFile(const std::string& path) {

            std::string contents;
            std::ifstream ifs(path);

            if (ifs) {
                ifs.seekg(0, std::ios::end);
                contents.resize(ifs.tellg());
                ifs.seekg(0, std::ios::beg);
                ifs.read(&contents[0], contents.size());
                ifs.close();
            }

            return contents;
        }

        void Shader::unload() {
            glDetachShader(_program, _vertexShader);
            glDetachShader(_program, _fragmentShader);

            glDeleteShader(_vertexShader);
            glDeleteShader(_fragmentShader);

            glDeleteProgram(_program);
        }

        void Shader::reportShaderError(GLuint shader) {
            GLint maxLength = 0;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
            std::string errorLog;
            errorLog.resize(maxLength);

            glGetShaderInfoLog(shader, maxLength, &maxLength, &errorLog[0]);

            gLogError << errorLog;

        }

        template<typename T>
        void Shader::setUniform(std::string name, T value) {
            throw NotImplementedException{};
        }

        template<>
        void Shader::setUniform(std::string name, Matrix4f value) {
            int location = glGetUniformLocation(_program, name.c_str());
            if (location != 0) {
                glUniformMatrix4fv(location, 1, GL_FALSE, &value[0]);
            }
        }
    }

}
