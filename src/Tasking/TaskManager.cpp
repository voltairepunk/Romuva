//
// Created by kestutis on 18.6.7.
//

#include "TaskManager.hpp"
#include "../Core/Logging/Logger.hpp"

namespace Romuva {

    namespace Tasking {

        TaskManager::TaskManager(size_t numWorkers) : _numWorkers(numWorkers), _isRunning(false), _stopping(false) {
            if (_numWorkers == 0) {
                _numWorkers = tbb::tbb_thread::hardware_concurrency() + 1;
            }

            if (_numWorkers < 1) {
                _numWorkers = 1;
            }
        }

        void TaskManager::addWork(Romuva::Tasking::Task t, bool repeating, bool background) {
            this->addWork(make_wrapped(std::move(t), repeating, background));
        }

        void TaskManager::addRepeatingWork(Romuva::Tasking::Task t, bool background) {
            this->addWork(std::move(t), true, background);
        }

        void TaskManager::addBackgroundWork(Romuva::Tasking::Task t, bool repeating) {
            this->addWork(std::move(t), repeating, true);
        }

        void TaskManager::addRepeatingBackgroundWork(Task t) {
            this->addWork(std::move(t), true, true);
        }

        void TaskManager::start() {
            if (this->_isRunning) return;

            this->_isRunning = true;

            gLog << "Starting Task Manager with " << this->_numWorkers << " background threads";

            // For background tasks
            for (size_t i = 0; i < this->_numWorkers; ++i) {
                this->_threads.push_back(
                        new tbb::tbb_thread([this]{
                            while(_isRunning) {
                                WrappedTask t;
                                bool popResult = _backgroundTasks.try_pop(t);
                                if (popResult) execute(t);
                            }
                        })
                        );
            }

            // For tasks on the main thread
            while(this->_isRunning) {
                TaskQueue localQueue;
                this->_mainTasks.swap(localQueue);
                WrappedTask t;
                while (localQueue.try_pop(t)) {
                    this->execute(t);
                }
            }
        }

        void TaskManager::stop() {

            this->addBackgroundWork([this]{this->_isRunning.store(false);}, false);
            for (const auto& it: this->_threads) {
                it->join();
            }
        }

        void TaskManager::addWork(WrappedTask t)
        {
            if (t.isBackground())
            {
                this->_backgroundTasks.push(t);
            }
            else {
                this->_mainTasks.push(t);
            }
        }

        void TaskManager::execute(WrappedTask t)
        {
            t();

            if (t.isRepeating())
            {
                this->addWork(std::move(t));
            }
        }

    }

}
