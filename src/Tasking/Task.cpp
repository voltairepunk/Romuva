//
// Created by kestutis on 18.6.7.
//

#include "Task.hpp"
#include "../Core/Logging/Logger.hpp"

namespace Romuva {

    namespace Tasking {

        WrappedTask::WrappedTask() {

        }

        WrappedTask::WrappedTask(Task t, bool repeating, bool background,
                                 unsigned short priority) :
            _unwrappedTask(std::move(t)),
            _isRepeating(repeating),
            _isBackground(background),
            _priority(priority)
        {

        }

        void WrappedTask::operator()() const {
            try {
                this->_unwrappedTask();
            } catch (const std::exception& ex) {
                gLogError << "Wrapped task execution gave an expression: " << ex.what();
            } catch (...) {
                gLogError << "Unkown exception occured";
            }
        }

        bool WrappedTask::isRepeating() const { return this->_isRepeating; }
        void WrappedTask::setRepeating(bool enabled) { this->_isRepeating = enabled; }
        bool WrappedTask::isBackground() const { return this->_isBackground; }
        void WrappedTask::setBackground(bool enabled) { this->_isBackground = enabled; }
        void WrappedTask::setPriority(unsigned short priority) { this->_priority = priority; }
        unsigned short WrappedTask::getPriority() const { return this->_priority; }

        bool CompareWrappedTasks::operator()(const WrappedTask &t1,
                                             const WrappedTask &t2) const {
            return t1._priority > t2._priority;
        }

        WrappedTask make_wrapped(
                Task task,
                bool repeating,
                bool background,
                unsigned short priority
        ) {
            return WrappedTask{ task, repeating, background, priority };
        }

    }

}

