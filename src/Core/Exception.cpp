//
// Created by kestutis on 18.6.8.
//

#include "Exception.hpp"
#include "Logging/Logger.hpp"

namespace Romuva {

    namespace Core {

        Exception::Exception(std::string description) : _description(std::move(description)) {}

        const char* Exception::what() const noexcept {
            std::string message = " Exception: ";

            if (this->_description.length() > 0) {
                message.append(this->_description);
            } else {
                message.append("Not defined");
            }

            gLogError << message;

            return message.c_str();
        }

    }

}