//
// Created by kestutis on 18.6.7.
//

#ifndef ROMUVA_ACTIVE_HPP
#define ROMUVA_ACTIVE_HPP

#include <memory>
#include <functional>

#include <tbb/concurrent_queue.h>
#include <tbb/tbb_thread.h>

namespace Romuva {

    namespace Core {

        typedef std::function<void()> Callback;


        class Active;

        struct ActiveThreadExecutor {
            void operator()(Active* active);
        };

        class Active {
        private:

            Active();

        public:

            typedef tbb::concurrent_queue<Callback> MessageQueue;

            Active(const Active&) = delete;
            Active& operator= (const Active&) = delete;

            ~Active();

            static std::unique_ptr<Active> create();

            void send(Callback message);

        protected:

            bool _isDone;
            MessageQueue _messageQueue;
            tbb::tbb_thread* _thread;

            ActiveThreadExecutor _atExecutor;
            friend struct ActiveThreadExecutor;
        };

    }

}

class Active {

};


#endif //ROMUVA_ACTIVE_HPP
