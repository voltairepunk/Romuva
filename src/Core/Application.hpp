//
// Created by kestutis on 18.6.11.
//

#ifndef ROMUVA_APP_HPP
#define ROMUVA_APP_HPP

#include "Module.hpp"

namespace Romuva {

    namespace Core {

        class Application : public Module
        {
        public:

            Application(std::string name) : Module(name, true, true) {};

            virtual bool init() = 0;
            virtual void update() = 0;
            virtual void shutdown() = 0;
        };

    }

}


#endif //ROMUVA_APP_HPP
