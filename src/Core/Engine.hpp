//
// Created by kestutis on 18.6.8.
//

#ifndef ROMUVA_ENGINE_HPP
#define ROMUVA_ENGINE_HPP

#include <memory>
#include <vector>
#include <map>

#include "../Tasking/TaskManager.hpp"
#include "../Messaging/Channel.hpp"
#include "Exception.hpp"

namespace Romuva {

    namespace Core {

        class Module;
        class Application;

        class Engine {

        public:

            //using Handler = std::function < void(const T&) > ;
            using ModulePtr = std::unique_ptr<Module>;
            using ModuleList = std::vector<ModulePtr>;
            using ModuleMap = std::map<std::string, Module*>;

            Engine();

            void setApplication(Application* app);

            void addModule(Module* module);

            template <typename T>
            T* getModule() const;

            void removeModule(std::string name);
            void removeModule(Module* module);

            bool init();
            void run();
            void shutdown();

            template <typename Func>
            void scheduleCallback(Func f, bool repeating = false, bool background = false) {
                this->_taskManager.addWork(f, repeating, background);
            }

            struct OnStop {};
            void operator()(const OnStop&);

        protected:

            void setApplication(std::unique_ptr<Application>&& application);

            void addModule(std::unique_ptr<Module>&& module);

            bool initialiseModules();
            void shutdownModules();

            ModuleList _modules;
            ModuleMap _moduleLookup;
            Tasking::TaskManager _taskManager;

            std::unique_ptr<Application> _application;
        };

        extern Engine gEngine;

        template <typename T>
        T* Engine::getModule() const
        {
            T* result = nullptr;

            for (const auto& system : _modules)
            {
                result = dynamic_cast<T*>(system.get());

                if (result)
                    return result;
            }

            throw Exception{"Module type not found"};
        }

    }

}


#endif //ROMUVA_ENGINE_HPP
