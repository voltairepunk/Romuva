//
// Created by kestutis on 18.6.11.
//

#include "Module.hpp"
#include "Logging/Logger.hpp"
#include "Engine.hpp"

namespace Romuva {

    namespace Core {

        Module::Module(std::string name, bool repeating, bool backround) : _name(std::move(name)), _repeating(repeating), _background(backround) {

        }

        bool Module::init() {
            gLog << "Initialising " << this->getName();
            this->_engine->scheduleCallback<std::function<void(void)>>([this]{this->update();}, _repeating, _background);
            return true;
        }

        void Module::update() {

        }

        void Module::shutdown() {
            gLog << "Shutting down " << this->getName();
        }

    }

}
