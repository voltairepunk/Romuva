//
// Created by kestutis on 18.6.8.
//

#ifndef ROMUVA_EXCEPTION_HPP
#define ROMUVA_EXCEPTION_HPP

#include <string>
#include <exception>

namespace Romuva {

    namespace Core {

        class Exception : public std::exception {
        public:

            Exception(std::string description);

            const char* what() const noexcept override;

        protected:

            std::string _description;
        };

    }

}



#endif //ROMUVA_EXCEPTION_HPP
