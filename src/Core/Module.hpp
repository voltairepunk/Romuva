//
// Created by kestutis on 18.6.11.
//

#ifndef ROMUVA_MODULE_HPP
#define ROMUVA_MODULE_HPP

#include "Logging/Logger.hpp"
#include "../Messaging/Channel.hpp"
#include "../Messaging/EventQueue.hpp"


namespace Romuva {

    namespace Core {

        class Engine;

        class Module {
        public:

            Module(std::string name, bool backround, bool repeating);
            virtual ~Module() {}


            virtual bool init();
            virtual void update();
            virtual void shutdown();

            std::string getName() { return this->_name; }

        private:

            Module(const Module& copy) = delete;
            const Module& operator= (const Module&) = delete;

        protected:

            Module() {}

            friend class Engine;

            Engine* _engine;
            std::string _name;

            bool _repeating;
            bool _background;


        };

    }

}




#endif //ROMUVA_MODULE_HPP
