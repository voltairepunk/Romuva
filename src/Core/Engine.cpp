//
// Created by kestutis on 18.6.8.
//

#include "Engine.hpp"

#include "Logging/Logger.hpp"
#include "Module.hpp"
#include "Application.hpp"

#include <assert.h>


namespace Romuva {

    namespace Core {

        Engine::Engine() {
            Messaging::Channel::add<OnStop>(this);
        }

        void Engine::setApplication(Application *app) {
            this->setApplication(std::unique_ptr<Application>(app));
        }

        void Engine::setApplication(std::unique_ptr<Application> &&application) {
            this->_application = std::move(application);

            if (this->_application) {
                this->_application->_engine = this;
                gLog << "Application set: " << this->_application->getName();
            }
        }

        void Engine::addModule(Module *module) {
            this->addModule(std::unique_ptr<Module>(module));
        }

        void Engine::addModule(std::unique_ptr<Module> &&module) {
            auto it = this->_moduleLookup.find(module->getName());

            if (it == this->_moduleLookup.end()) {
                module->_engine = this;
                this->_moduleLookup[module->getName()] = module.get();
                this->_modules.emplace_back(std::move(module));
            } else {
                throw Exception{std::string("Duplicate module being added to engine: ").append(module->getName())};
            }
        }

        void Engine::removeModule(std::string name) {
            Module* m = nullptr;

            auto it = this->_moduleLookup.find(name);
            if (it != this->_moduleLookup.end()) {
                m = it->second;
            }

            if (m) this->removeModule(m);
            else {
                gLogError << "Trying to remove a module that is not registered: " << name;
            }
        }

        void Engine::removeModule(Module *module) {
            auto it = std::find_if(
                    this->_modules.begin(),
                    this->_modules.end(),
                    [module](const ModulePtr& ptr) {
                        return ptr.get() == module;
                    }
                    );

            if (it != this->_modules.end()) {
                module->shutdown();
                this->_modules.erase(it);

                for (auto jt = this->_moduleLookup.begin(); jt != this->_moduleLookup.end(); ++jt) {
                    if (jt->second == module) {
                        this->_moduleLookup.erase(jt);
                        break;
                    }
                }
            }
        }

        bool Engine::init() {
            // Load some sort of settings
            this->initialiseModules();
            return true;
        }

        void Engine::run() {
            if (this->_application) {
                if (!this->_application->init()) {
                    gLogFatal << "Failed to initialise application: " << this->_application->getName();
                    return;
                }
            } else {
                gLogWarning << "Application not set";
            }

            this->_taskManager.start();
        }

        void Engine::shutdown() {
            this->_taskManager.stop();
            this->shutdownModules();
        }

        bool Engine::initialiseModules() {
            for (const auto& module : this->_modules) {
                if (!module->init()) {
                    gLogFatal << "Could not initialise module: " << module->getName();
                    return false;
                }
            }

            return true;
        }

        void Engine::shutdownModules() {
            if (this->_application) {
                this->_application->shutdown();
            }

            for (const auto& module : this->_modules) {
                module->shutdown();
            }

            this->_modules.clear();
            this->_moduleLookup.clear();
        }

        void Engine::operator()(const OnStop &) {
            this->shutdown();
        }

        Engine gEngine;

    }

}