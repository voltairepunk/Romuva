//
// Created by kestutis on 18.6.7.
//

#include "Active.hpp"

#include <utility>

namespace Romuva {

    namespace Core {

        void ActiveThreadExecutor::operator()(Active *active) {
            while (!active->_isDone)
            {
                Callback fn;
                bool gotOne = active->_messageQueue.try_pop(fn);

                if (gotOne) fn();
            }
        }

        Active::Active() : _isDone(false), _atExecutor() {}

        Active::~Active() {
            send([this] { _isDone = true; });
            this->_thread->join();
            delete this->_thread;
        }

        std::unique_ptr<Active> Active::create() {
            std::unique_ptr<Active> result(new Active);
            result->_thread = new tbb::tbb_thread(result->_atExecutor, result.get());
            return result;
        }

        void Active::send(Romuva::Core::Callback message) {
            this->_messageQueue.push(message);
        }

    }

}