//
// Created by voltaire on 5/4/19.
//

#include "Transform.hpp"

namespace Romuva {

    namespace Components {


        Transform::Transform(const Vector3f& position, const Vector3f& up, const Vector3f& forward) :
        _position(position), _up(up), _forward(forward) {}

        const Vector3f &Transform::getPosition() {
            return _position;
        }

        const Vector3f &Transform::getUp() {
            return _up;
        }

        const Vector3f &Transform::getForward() {
            return _forward;
        }
    }

}