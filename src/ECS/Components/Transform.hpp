//
// Created by voltaire on 5/4/19.
//

#ifndef ROMUVA_TRANSFORM_HPP
#define ROMUVA_TRANSFORM_HPP

#include <ECS/ECS.hpp>
#include <Util/Mappings.hpp>

namespace Romuva {

    namespace Components {

        class Transform : public ECS::Component {

        public:

            Transform(const Vector3f& position, const Vector3f& up, const Vector3f& forward);

            ~Transform() override = default;

            const Vector3f& getPosition();
            const Vector3f& getUp();
            const Vector3f& getForward();

        protected:

            Vector3f _position;
            Vector3f _up;
            Vector3f _forward;

        };

    }

}


#endif //ROMUVA_TRANSFORM_HPP
