//
// Created by voltaire on 5/4/19.
//

#ifndef ROMUVA_CAMERA_HPP
#define ROMUVA_CAMERA_HPP

#include <ECS/ECS.hpp>

#include <Util/Mappings.hpp>

namespace Romuva {

    class Camera : public ECS::Component {

    public:

        enum Type {
            PERSPECTIVE,
            ORTHO
        };

        Camera(bool main, float fov, float near, float far, Type type);

        ~Camera() override = default;

        void update() override;

        const Matrix4f& getView();
        const Matrix4f& getProjection();

        const bool& isMain();
        const float& getFov();
        const float& getNear();
        const float& getFar();
        const Type& getType();

    protected:

        void recalculateViewProjection();

        bool _main;
        float _fov;
        float _near;
        float _far;
        Type _type;

        Matrix4f _view;
        Matrix4f _projection;
    };
}


#endif //ROMUVA_CAMERA_HPP
