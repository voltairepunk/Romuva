//
// Created by voltaire on 5/4/19.
//

#include "Camera.hpp"

#include <ECS/Components/Transform.hpp>

namespace Romuva {


    Camera::Camera(bool main, float fov, float near, float far, Camera::Type type) :
    _main(main), _fov(fov), _near(near), _far(far), _type(type) {

    }

    void Camera::update() {
        recalculateViewProjection();
        Component::update();
    }

    const Matrix4f &Camera::getView() {
        return _view;
    }

    const Matrix4f &Camera::getProjection() {
        return _projection;
    }

    const bool &Camera::isMain() {
        return _main;
    }

    const float &Camera::getFov() {
        return _fov;
    }

    const float &Camera::getNear() {
        return _near;
    }

    const float &Camera::getFar() {
        return _far;
    }

    const Camera::Type &Camera::getType() {
        return _type;
    }

    void Camera::recalculateViewProjection() {
        auto transform = entity->getComponent<Components::Transform>();

        _view = Matrix4f::LookAt(transform.getForward(), transform.getPosition(), transform.getUp(), 1.0);
        int width = 1280;
        int height = 720;

        switch(_type)
        {
            case PERSPECTIVE:
            {
                _projection = Matrix4f::Perspective(
                        _fov,
                        (float)width / (float)height,
                        _near,
                        _far,
                        1.0
                        );
            } break;
            case ORTHO:
            {
                _projection = Matrix4f::Ortho(
                        0.0f,
                        (float) width,
                        (float) height,
                        0.0f,
                        _near,
                        _far
                        );
            } break;
        }
    }
}