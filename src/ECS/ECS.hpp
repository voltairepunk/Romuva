//
// Created by kestutis on 18.6.12.
//

#ifndef ROMUVA_ECS_HPP
#define ROMUVA_ECS_HPP

#include <vector>
#include <memory>
#include <algorithm>
#include <bitset>
#include <array>

namespace Romuva {

    namespace ECS {

        class Component;
        class Entity;
        class Manager;

        using ComponentID = std::size_t;
        using Group = std::size_t;

        inline ComponentID getNewComponentTypeID() {
            static ComponentID lastID = 0u;
            return lastID++;
        }

        template <typename T>
        inline ComponentID getComponentTypeID() noexcept {
            static_assert(std::is_base_of<Component, T>::value, "");
            static ComponentID typeID = getNewComponentTypeID();
            return typeID;
        }

        constexpr std::size_t maxComponents = 32;
        constexpr std::size_t maxGroups = 32;

        using ComponentBitSet = std::bitset<maxComponents>;
        using GroupBitSet = std::bitset<maxGroups>;

        using ComponentArray = std::array<Component*, maxComponents>;

        class Component
        {
        public:

            Entity* entity;

            Component() : entity(nullptr) {}

            virtual void init() {}
            virtual void update() {}
            virtual void draw() {}
            virtual ~Component() = default;
        };

        class Entity
        {
        private:

            Manager& _manager;
            bool _active = true;
            std::vector<std::unique_ptr<Component>> _components;

            ComponentArray _componentArray;
            ComponentBitSet  _componentBitset;
            GroupBitSet _groupBitset;

        public:

            explicit Entity(Manager& manager) : _componentArray{}, _manager(manager) {}

            Entity(const Entity& copy) = delete;

            void update()
            {
                for (auto& c: this->_components) c->update();
            }

            void draw()
            {
                for (auto& c: this->_components) c->draw();
            }

            bool isActive() const { return this->_active; }
            void destroy() { this->_active = false; }

            bool hasGroup(Group group) {
                return this->_groupBitset[group];
            }

            void addGroup(Group group);

            void delGroup(Group group) {
                this->_groupBitset[group] = false;
            }

            template <typename T>
            bool hasComponent() const {
                return this->_componentBitset[getComponentTypeID<T>()];
            };

            template <typename T, typename... TArgs>
            T& addComponent(TArgs&&... args) {
                T* c(new T(std::forward<TArgs>(args)...));
                c->entity = this;
                std::unique_ptr<Component> uPtr { c };
                this->_components.emplace_back(std::move(uPtr));

                this->_componentArray[getComponentTypeID<T>()] = c;
                this->_componentBitset[getComponentTypeID<T>()] = true;

                c->init();
                return *c;
            };

            template <typename T>
            T& getComponent() const {
              auto ptr(this->_componentArray[getComponentTypeID<T>()]);
              return *static_cast<T*>(ptr);
            };
        };

    }

}

#endif //ROMUVA_ECS_HPP
