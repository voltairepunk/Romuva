//
// Created by kestutis on 18.6.12.
//

#include "Manager.hpp"
#include "../Core/Engine.hpp"

namespace Romuva {

    namespace ECS {

        Manager::Manager() : Module("ECSManager", true, true) {
            _background = true;
            _repeating = true;
        }

        Manager::~Manager() = default;

        bool Manager::init() {
            this->_engine->scheduleCallback<std::function<void(void)>>([this]{this->refresh();}, true, true);

            return Module::init();
        }

        void Manager::update() {
            for (auto& e : this->_entities) e->update();
            this->refresh();
        }

        void Manager::shutdown() {
            for (auto& e : this->_entities) e->destroy();
            this->refresh();
        }

        void Manager::draw() {
            for (auto& e : this->_entities) e->draw();
        }

        void Manager::refresh() {
            for (auto i(0u); i < maxGroups; i++) {
                auto& v(this->_groupedEntities[i]);
                v.erase(
                        std::remove_if(std::begin(v), std::end(v),
                        [i](Entity* entity) {
                            return !entity->isActive() || !entity->hasGroup(i);
                        }),
                        std::end(v));
            }

            this->_entities.erase(std::remove_if(std::begin(this->_entities), std::end(this->_entities),
            [] (const std::unique_ptr<Entity>& entity) {
                return !entity->isActive();
            }), std::end(this->_entities));
        }

        void Manager::addToGroup(Entity *entity, Group group) {
            this->_groupedEntities[group].emplace_back(entity);
        }

        std::vector<Entity*>& Manager::getGroup(Group group) {
            return this->_groupedEntities[group];
        }

        Entity& Manager::addEntity() {
            auto* e = new Entity(*this);
            std::unique_ptr<Entity> uPtr{ e };
            this->_entities.emplace_back(std::move(uPtr));
            return *e;
        }

    }

}