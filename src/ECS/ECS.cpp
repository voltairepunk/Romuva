//
// Created by kestutis on 18.6.13.
//

#include "ECS.hpp"
#include "Manager.hpp"

namespace Romuva {

    namespace ECS {

        void Entity::addGroup(Group group) {
            this->_groupBitset[group] = true;
            this->_manager.addToGroup(this, group);
        }

    }

}
