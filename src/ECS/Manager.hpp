//
// Created by kestutis on 18.6.12.
//

#ifndef ROMUVA_MANAGER_HPP
#define ROMUVA_MANAGER_HPP

#include "../Core/Module.hpp"
#include "ECS.hpp"

namespace Romuva {

    namespace ECS {

        class Manager : public Core::Module
        {
        private:

            std::vector<std::unique_ptr<Entity>> _entities;
            std::array<std::vector<Entity*>, maxGroups> _groupedEntities;

        public:

            Manager();
            ~Manager() override;

            bool init() override;
            void update() override;
            void shutdown() override;

            void draw();

            void refresh();

            void addToGroup(Entity* entity, Group group);

            std::vector<Entity*>& getGroup(Group group);

            Entity& addEntity();
        };

    }

}

#endif //ROMUVA_MANAGER_HPP
