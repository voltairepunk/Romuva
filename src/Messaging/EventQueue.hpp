//
// Created by kestutis on 18.6.7.
//

#ifndef ROMUVA_EVENTQUEUE_HPP
#define ROMUVA_EVENTQUEUE_HPP

#include <algorithm>
#include <functional>
#include <iostream>
#include <tbb/mutex.h>
#include <vector>
#include <utility>
#include <tbb/concurrent_queue.h>

namespace Romuva {

    namespace Messaging {

        class System;

        struct Event {

            std::function<void(const System* system)> handler;

        };

        template<typename T>
        class EventQueue
        {
        public:

            using Handler = std::function < void(const T&) > ;

            static EventQueue& instance();

            bool PollEvents(Handler& handler);
            bool PollEvents(Event& evt);

            void QueueEvent(const Handler& handler);
            void QueueEvent(const Event& evt);

        protected:

            EventQueue();

            tbb::concurrent_queue<Handler> _eventHandlerQueue;
            tbb::concurrent_queue<Event> _eventQueue;

        };

        template <typename T>
        EventQueue<T>& EventQueue<T>::instance()
        {
            static EventQueue instance;
            return instance;
        }

        template <typename T>
        bool EventQueue<T>::PollEvents(Handler& handler)
        {
            return this->_eventHandlerQueue.try_pop(handler);
        }

        template <typename T>
        bool EventQueue<T>::PollEvents(Event& evt)
        {
            return this->_eventQueue.try_pop(evt);
        }

        template <typename T>
        void  EventQueue<T>::QueueEvent(const Handler& handler)
        {
            this->_eventHandlerQueue.push(handler);
        }

        template <typename T>
        void  EventQueue<T>::QueueEvent(const Event& evt)
        {
            this->_eventQueue.push(evt);
        }

        template <typename T>
        EventQueue<T>::EventQueue()
        {

        }

    }

}


#endif //ROMUVA_EVENTQUEUE_HPP
