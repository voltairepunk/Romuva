# Romuva

A hobby project to create a graphics engine


## Requirements

### Linux
* xorg-dev 
* libglu1-mesa-dev

## Build instructions

### Get the repository

* `git clone <URL>`
* `git submodule update --init --recursive`

### Linux

#### Install additional required libraries

* `sudo apt install xorg-dev libglu1-mesa-dev`

#### Prepare luajit
* `cd vendor/luajit`
    * `make`
    * `sudo make install`
* `cd ../../`


* ... Run cmake ...
